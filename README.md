# Programación Distribuida 

### API GATEWAY
Como usar:
>```sh
>$ docker-compose build
>$ docker-compose up
>```

Contenedores
>```sh
>$ docker ps
>```
>
>```sh
>CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                                                    NAMES
>32dcbbee1972        nginx                        "/docker-entrypoint.…"   15 minutes ago      Up 15 minutes       0.0.0.0:8000->8000/tcp, 80/tcp, 0.0.0.0:8800->8800/tcp   auth_api_node_nginx_app_1
>3054cb3c6483        auth_api_node_users_api      "docker-entrypoint.s…"   15 minutes ago      Up 15 minutes       0.0.0.0:4000->80/tcp                                     auth_api_node_users_api_1
>26b7af1bceec        auth_api_node_delivery_api   "docker-entrypoint.s…"   30 minutes ago      Up 15 minutes       0.0.0.0:4001->80/tcp                                     auth_api_node_delivery_api_1
>e2cd37958d61        mongo                        "docker-entrypoint.s…"   30 minutes ago      Up 15 minutes       0.0.0.0:32776->27017/tcp                                 auth_api_node_db_2_1
>65c43f4ab577        mongo                        "docker-entrypoint.s…"   30 minutes ago      Up 15 minutes       0.0.0.0:32777->27017/tcp                                 auth_api_node_db_1_1
>```
----

### Crear un usuario

  **POST**|_url_ | http://localhost:8000/delivery/user/app/signup
--|--|--

Body|
--|
```json
{
    "username": "Wonky",
    "password": "123456",
    "passwordConfirmation": "123456",
    "email": "user1@example.com",
    "emailConfirmation": "user1@example.com",
}
```

Request|
--|
>```json
>{
>    status:"OK",
>    "message": "El usuario agregado exitosamente!!!"
>}
>```
---

### Usuario login
  **POST**|_url_ | http://localhost:8000/delivery/user/app/login
--|--|--

Body|
--|
>```json
>{
>    email:"user1@example.com",
>    password"123456"
>}
>```

Request|
--|

>```json
>    {
>        "status": "Ok",
>        "message": "El usuario ha sido autenticado!!!",
>        "data": {
>            "user": {
>                "_id": "5fc41688f43c4c001376009a",
>                "nombre": "Tritonar",
>                "email": "dane@jupyter.com",
>                "password": "$2b$10$um/CsFQTpJcpcpE7PGlCO.eu/myeBGlJQOMo6c/urhHQkpxtVGoBu",
>                "__v": 0
>            },
>            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYzQxNjg4ZjQzYzRjMDAxMzc2MDA5YSIsImlhdCI6MTYwNjY4NjM2OSwiZXhwIjoxNjA2Njg5OTY5fQ.D5wClDrT3IOU_tWkkFIJmorAViThvwz26SWaomOxMvk"
>        }
>    }
>```
---
### Vista protegida:
  **POST**|_url_ | http://localhost:8000/delivery/app/order
--|--|--
**Requiere token**
Request|STATUS | 200 OK
--|--|--
```json
{
    "status": "error",
    "message": "jwt must be provided",
    "data": null
}
```
---
**Consultar el estado de usuario delivery**

  **GET**|_url_ | http://localhost:8000/delivery/app/status
--|--|--

**Requiere token**

Request|STATUS | 200 OK
--|--|--
```json
{
    "status": "error",
    "message": "jwt must be provided",
    "data": null
}
```

**Cambiar el estado de usuario usando token**

**POST**|http://localhost:8000/delivery/app/status
---|---

>HEADER| |
>--|--
>_KEY_|_VALUE_|
>x-access-token|eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYzQxNjg4ZjQzYzRjMDAxMzc2MDA5YSIsImlhdCI6MTYwNjY4NjM2OSwiZXhwIjoxNjA2Njg5OTY5fQ.D5wClDrT3IOU_tWkkFIJmorAViThvwz26SWaomOxMvk

Request|STATUS | 404 Not found
--|--|--

```html
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Error</title>
</head>

<body>
	<pre>Cannot POST /status</pre>
</body>

</html>
```

---
**Consultar el estado del usuario usando token**

>HEADER| |
>--|--
>_KEY_|_VALUE_|
>x-access-token|TOKEN_ARRAY

#### Por ejemplo
>**GET**|http://localhost:8000/delivery/app/order
>---|---
>
>HEADER| |
>--|--
>_KEY_|_VALUE_|
>x-access-token|eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYzQxNjg4ZjQzYzRjMDAxMzc2MDA5YSIsImlhdCI6MTYwNjY4NjM2OSwiZXhwIjoxNjA2Njg5OTY5fQ.D5wClDrT3IOU_tWkkFIJmorAViThvwz26SWaomOxMvk
>
Request|STATUS | 200 OK
--|--|--
>```json
>[]
>```

---
