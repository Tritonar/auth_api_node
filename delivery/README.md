# Delivery / Pickup

- Creación de delivery/pickup
- Chequear disponibilidad
- Habilitar/deshabilitar delivery/pickup
- Notificar nuevo viaje (ver de llevar a un queue)
- Aceptar/Declinar viajes  (cuando se acepta se inicia un viaje)
- Finalizar estado de viajes (cambia el estado de un viaje) 
- Delivery que más viajes tiene
- Días y horas en donde menos personas para delivery hay disponibles
- Consulta de delivery/pickup por alquiler 
## Rutas
- [X] /delivery/order/{Id} [GET]
- [X] /delivery/order [PUT]
- [ ] /delivery/disponibilidad [GET]
- [ ] /delivery/estado [POST]
- [ ] /delivery/viaje/{alquilerId} [POST](aceptado=true/false) 48hs y la fecha
- [ ] /delivery/viaje/{alquilerId}/finalizar [POST]
- [ ] /delivery/metrica/viajes [GET]
- [ ] /delivery/metrica/disponibilidad [GET]




Algunos detalles:

| Path | Verbo | Parámetros | Comentarios |
| ------ | ------ |------ | ------ |
| /delivery | PUT | <ul><li>username:String </li><li>password:String</li><li>emal:String</li><li>emailConfirmacion:String</li></ul>| Devuelve un token único para ese delivery |
| /delivery/status | POST | activo:boolean | Establece la habilitación o inhabilitación del usuario(el username se obtiene del token |
| /delivery/order/{alquilerId} | POST | aceptado:boolean | Establece si el delivery acepta o rechaza el viaje |
| /delivery/viaje/{alquilerId}/finalizar | POST | cell | Cambia el estado del viaje a “finalizado” |


---


## Order delivery

Crear Order|
--|

  **POST**|_url_ | http://localhost:4000/delivery/order
--|--|--


```json
    {
        "status": "ENTREGADO",
        "rentId": "44564-a457ed-0321a7-aec678",
        "requested_on": "2020-11-01T00:00:00.000Z"

    }
```

**Response**| | STATUS | 200
--|--|--|--


```json
{
    "status": "ENTREGADO",
    "_id": "5fc574ce12906d94ad8c403a",
    "rentId": "44564-a457ed-0321a7-aec678",
    "requested_on": "2020-11-01T00:00:00.000Z",
    "__v": 0
}
```
---

Get Order|
---|

  **GET**|_url_ | http://localhost:4000/delivery/order/5fc574ce12906d94ad8c403a
--|--|--

```json

```
**Response**| | STATUS | 200
--|--|--|--
```json
{
    "status": "ENTREGADO",
    "_id": "5fc574ce12906d94ad8c403a",
    "rentId": "44564-a457ed-0321a7-aec678",
    "requested_on": "2020-11-01T00:00:00.000Z",
    "__v": 0
}
```

---

Delete Order|
---|

  **DELETE**|_url_ | http://localhost:4000/delivery/order/5fc573992c672ca1525d593e
--|--|--

```json

```

**Response**| | STATUS | 200
--|--|--|--

```json
{
    "message": "Order deleted successfully!"
}
```
---


Delivery|
--------- |

Path | Verbo | Parámetros | Comentarios
-----|-------|------------|------------
/delivery|PUT| <ul><li>username:String </li><li>password:String</li><li>emal:String</li><li>emailConfirmacion:String</li></ul>| Devuelve un token único para ese delivery| from the first column|
/delivery/status|POST|activo:boolean|Establece la habilitación o inhabilitación del usuario(el username se obtiene del token)
/delivery/order/{id}|POST|aceptado:boolean|Establece si el delivery acepta o rechaza el viaje
/delivery/order/{id}/end|POST||Cambia el estado del viaje a “finalizado”
