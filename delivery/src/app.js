const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const pkg = require("../package.json");
const jwt = require('jsonwebtoken');

// settings
const app = express();
app.set('port', process.env.PORT || 4000);

// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());



// Routes
app.get("/", (req, res) => {
  res.json({
    message: "Welcome to Delivery API ",
    name: pkg.name,
    version: pkg.version,
    description: pkg.description,
    author: pkg.author,
  });
});

app.set('secretKey', 'programacion_distribuida_2_delivery');
function validateUser(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      res.json({status:"error", message: err.message, data:null});
    }else{
      // add user id to request
      req.body.userId = decoded.id;
      next();
    }
  });
  
}
app.use('/status',validateUser, require('./routes/agent.routes'));

app.use('/order', validateUser,require('./routes/orders.routes'));

module.exports = app;
