// Cargamos el módulo de mongoose
const mongoose = require('mongoose');
//Definimos el esquema
const Schema = mongoose.Schema;
// Creamos el objeto del esquema con sus correspondientes campos
const DeliverySchema = new Schema({
agentId: {
    desc: "The id to rent film.",
    type: String,
    trim: true,  
    required: true,
    unique: true,
    }, 
status: {
    desc: "Status order.",
    type: String,
    trim: true,
    required: true,
    enum: ["ACTIVO", "INACTIVO","EN_REPARTO"],
    default: "INACTIVO",
    },
login_on: {
    type: Date,
    trim: true
    },
logoff: {
        type: Date,
        trim: true
        }
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('Order', DeliverySchema)