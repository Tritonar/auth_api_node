const { Router } = require("express");
const router = Router();

router.get("/disponibilidad", (request, response) => {
  response.send("welcome");
});

router.get("/:alquilerId", (request, response) => {
  response.json(request.params.id);
});

router.post("/estado", (request, response) => {
  response.json(request.body);
});

router.post("/:alquilerId", (request, response) => {
  response.json(request.params.id, request.body);
});

router.post("/viaje/:alquilerId", (request, response) => {
  response.json(request.params.id, request.body);
});

router.put("/", (request, response) => {
  response.json( request.body);
});

router.delete("/:id", (request, response) => {
  response.json(request.params.id);
});

module.exports = router;