const { Router } = require("express");
const router = Router();


// Cargamos el controlador de order
const orderController = require('../controllers/order');

router.get('/', orderController.findAll);
router.post('/', orderController.create);
router.get('/:id', orderController.findOne);
router.put('/:id', orderController.UpdateOrder);
router.delete('/:id', orderController.delete);

module.exports = router;