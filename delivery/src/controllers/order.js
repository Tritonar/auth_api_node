const OrderModel = require("../models/order");


exports.create = (req, res) => {
/**
 * validation request
 */
    if (!req.body.rentId) {
      return res.status(400).send({
        message: "Required field can not be empty",
      });
    }

/**
 * Create a user
 */
  const order = new OrderModel({
    rentId: req.body.rentId,
    status: req.body.status,
    requested_on: req.body.requested_on,
  });
  order
    .save()
    .then((data) => {
      res.send(data);       
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Order.",
    });
    });
};


/**
 * Find all Order
 */
exports.findAll = (req, res) => {
    OrderModel.find()
      .sort({ rentId: -1 })
      .then((order_delivery) => {
        res.status(200).send(order_delivery);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Error Occured",
        });
      });
  };


/**
 * Find one Oder
 */
  exports.findOne = (req, res) => {
    OrderModel.findById(req.params.id)
      .then((order_delivery) => {
        if (!order_delivery) {
          return res.status(404).send({
            message: "Order not found with id " + req.params.id,
          });
        }
        res.status(200).send(order_delivery);
        console.log(order_delivery);
      })
      .catch((err) => {
        return res.status(500).send({
          message: "Error retrieving order with id " + req.params.id,
        });
      });
  };

/**
 * Delete a oder with the specified id in the request
 */
exports.delete = (req, res) => {
    OrderModel.findByIdAndRemove(req.params.id)
      .then((order_delivery) => {
        if (!order_delivery) {
          return res.status(404).send({
            message: "Order not found ",
          });
        }
        res.send({ message: "Order deleted successfully!" });
      })
      .catch((err) => {
        return res.status(500).send({
          message: "Could not delete order ",
        });
      });
  };
  
/**
 * Update a user with the specified id in the request
 */
exports.UpdateOrder= (req, res) => {
    if (!req.body.rentId || !req.body.status || !req.body.requested_on) {
      res.status(400).send({
        message: "required fields cannot be empty",
      });
    }
    OrderModel.findByIdAndUpdate(req.params.id, req.body, { new: true })
      .then((order) => {
        if (!order) {
          return res.status(404).send({
            message: "no order found",
          });
        }
        res.status(200).send(order);
      })
      .catch((err) => {
        return res.status(404).send({
          message: "error while updating the post",
        });
      });
  };