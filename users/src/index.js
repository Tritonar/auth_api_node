const mongoose = require('mongoose');
const app = require('./app');
mongoose.connect(
  process.env.DB_URL || '',
  { useNewUrlParser: true, useUnifiedTopology: true }
);

mongoose.connection.on('error', (err) => console.error('Error while connecting to DB: ', err));
mongoose.connection.once('open', async () => {
  console.log('Connected to DB.');
  await app.listen(app.get('port'));
  console.log('Server running on port', app.get('port'));
});
