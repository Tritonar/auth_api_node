// Cargamos el modulo express
const express = require('express');
const router = express.Router();
// Cargamos el controlador del usuario
const userController = require('../controllers/users');
// Especificamos nuestras rutas teniendo en cuenta los metodos creados en nuestro controlador
// , y especificando que seran rutas que usaran el metodo POST
router.post('/signup', userController.create);
router.post('/login', userController.authenticate);
module.exports = router;