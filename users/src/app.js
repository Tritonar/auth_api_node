
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const pkg = require("../package.json");
const bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
// settings
const app = express();
app.set('port', process.env.PORT || 4000);
// https://www.jsonwebtoken.io/
app.set('secretKey', 'programacion_distribuida_2_delivery'); // Clave Secreta para nuestro JWT
// middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.json());
app.use(cors());


// Routes
app.get("/", (req, res) => {
  res.json({
    message: "Welcome to User-Delivery API",
    name: pkg.name,
    version: pkg.version,
    description: pkg.description,
    author: pkg.author,
  });
});

/*
* validateUser export function
* Usar este codigo para validar en el servicio
function validateUser(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      res.json({status:"error", message: err.message, data:null});
    }else{
      // add user id to request
      req.body.userId = decoded.id;
      next();
    }
  });
  
}
*
*/

app.use('/app', require('./routes/users'));


/*
app.use('/payment', require('./routes/payments'));

app.use(function(err, req, res, next) {
  //console.log(err);
  
   if(err.status === 404)
    res.status(404).json({message: "Not found"});
   else 
     res.status(500).json({message: "Error interno en el servidor!!"});
 });
 */
/*
app.use((err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';

  res.status(err.statusCode).json({
    status: err.status,
    message: err.message
  });
});
 */
module.exports = app;
