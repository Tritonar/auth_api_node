// Cargamos el modelo recien creado
const userModel = require('../models/users');
// Cargamos el módulo de bcrypt
const bcrypt = require('bcrypt'); 
// Cargamos el módulo de jsonwebtoken
const jwt = require('jsonwebtoken');
const { isNull } = require('util');

// Operaciones permitidas Uuarios
module.exports = {
 create: function(req, res, next) {
  
  userModel.create({ username: req.body.username, email: req.body.email, password: req.body.password }, function (err, result) {
      if (err) 
       //next(err);
       {
        err.statusCode = err.statusCode || 500;
        err.status = err.status || 'error';
      
        res.status(err.statusCode).json({
          status: err.status,
          message: err.message
        });
      }


      else
       res.json({status: "pass", message: "Usuario agregado exitosamente!!!", data: null});
      
    });
 },
 // POST http://127.0.0.1:4000/user/login
 /*
 {
    "email":"user1@example.com",
    "password":"123456"
}
*/
authenticate: function(req, res, next) {

  userModel.findOne({email:req.body.email}, function(err, userInfo){
    if(userInfo)
    {
      if(!req.body.password|| !req.body.email)
      {
        res.json({status:"error", message: "Requieres email/password!!"});
  
      } else if (err) 
        {
        err.statusCode = err.statusCode || 500;
        err.status = err.status || 'error';
      
        res.status(err.statusCode).json({
          status: err.status,
          message: err.message
        });
        } 
        else {
      if(bcrypt.compareSync(req.body.password, userInfo.password)) {
        //const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), { expiresIn: '1h' });
        const token = jwt.sign({email: userInfo.email}, req.app.get('secretKey'), { expiresIn: '1h' });
        res.json({status:"Ok", message: "El usuario ha sido autenticado!!!", data:{user: userInfo, token:token}});
      }else{
        res.json({status:"error", message: "Invalid email/password!!", data:null});
      }
     }
    } else 
    {
      res.json({status:"error", message: "Invalid email/password!!"});

    } 
    
    });
 },

}

