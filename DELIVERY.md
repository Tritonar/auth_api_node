


## Usuarios de delivery

- [x] /delivery/user/app/signup `POST`
- [x] /delivery/user/app/login`POST`
- [ ] /delivery/app/status `POST`
- [ ] /delivery/app/order `GET`
- [ ] <del>/user/credit `POST` `GET`<del>
- [ ] <del> /user/credit <del>


Path | Verbo | Parámetros | Comentarios
-----|-------|------------|------------
/user/register|POST| <ul><li>username:_String_ </li><li>password:_String_</li><li>passwordConfirmacion:_String_</li><li>email:_String_</li><li>emailConfirmacion:_String_</li></ul>| Nuevo usuario|
/user/status|POST| <lu><li> activo:boolean </li></lu> |Establece la habilitación o inhabilitación del usuario(el username se obtiene del token)
/user/login|POST|<ul><li>email:_String_ </li><li>password:_String_</li></ul>|Devuelve un token único para ese usuario


----

Decode JWT
```js

const jwt_decode = require('jwt-decode');

var token = "eyJ0eXAiO.../// jwt token";
var decoded = jwt_decode(token);
console.log(decoded);
 
/* prints:
 * { foo: "bar",
 *   exp: 1393286893,
 *   iat: 1393268893  }
 */
/*
*/
var decodedHeader = jwt_decode(token, { header: true });
console.log(decodedHeader);

/* prints:
 * { typ: "JWT",
 *   alg: "HS256" }
 */

```
## header payload
```js

//  obtener la carga útil decodificada 
//ignorando la firma, no se necesita se
// get the decoded payload ignoring signature, no secretOrPrivateKey needed
var decoded = jwt.decode(token);
 
// get the decoded payload and header
//  obtener la carga útil decodificada y el encabezado
var decoded = jwt.decode(token, {complete: true});
console.log(decoded.header);
console.log(decoded.payload)

```

## Errores y códigos
- TokenExpiredError

Error object:| desc
-------|--
name:| 'TokenExpiredError'
message:| 'jwt expired'
expiredAt:| [ExpDate]

```js


jwt.verify(token, 'shhhhh', function(err, decoded) {
  if (err) {
    /*
      err = {
        name: 'TokenExpiredError',
        message: 'jwt expired',
        expiredAt: 1408621000
      }
    */
  }
});
```
- JsonWebTokenError

Error|JsonWebTokenError
--|--
name:| 'JsonWebTokenError'
message:|<lu><li>'jwt malformed'</li><li>'jwt signature is required'</li><li>'invalid signature'</li><li>'jwt audience invalid. expected: [OPTIONS AUDIENCE]'</li><li>'jwt issuer invalid. expected: [OPTIONS ISSUER]'</li><li>'jwt id invalid. expected: [OPTIONS JWT ID]'</li><li>'jwt subject invalid. expected: [OPTIONS SUBJECT]'</li></lu>
```js
jwt.verify(token, 'shhhhh', function(err, decoded) {
  if (err) {
    /*
      err = {
        name: 'TokenExpiredError',
        message: 'jwt expired',
        expiredAt: 1408621000
      }
    */
  }
});
```
- NotBeforeError

Error|`NotBeforeError`
--|--
arg|Error object:
name:| 'NotBeforeError'
message:| 'jwt not active'
date:| 2018-10-04T16:10:44.000Z

```js
jwt.verify(token, 'shhhhh', function(err, decoded) {
  if (err) {
    /*
      err = {
        name: 'NotBeforeError',
        message: 'jwt not active',
        date: 2018-10-04T16:10:44.000Z
      }
    */
  }
});
```

[source](https://www.npmjs.com/package/jsonwebtoken)
